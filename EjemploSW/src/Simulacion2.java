import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class Simulacion2 extends SwingWorker<Long, Long>
{
	private long _n; 
	private JTextField resultJTextField; 
	private JProgressBar barraProgreso;
	
	public Simulacion2(JTextField result, JProgressBar barra, int numero)
	{
		resultJTextField = result;
		barraProgreso = barra;
		_n = numero;
	}
	
	@Override
	public Long doInBackground() throws Exception 
	{
		barraProgreso.setMaximum((int) _n);
		barraProgreso.setMinimum(1);
		
		if(_n < 2) 
			return _n;

		long fn1 = 0, fn2 = 1, fibonacci = 0;
		for (int i = 2; i <= _n; i++) 
		{
			Thread.sleep(100);
			fibonacci = fn1 + fn2;
			publish(new Long[] { fibonacci });
			barraProgreso.setValue(i);			
			fn1 = fn2;
			fn2 = fibonacci;
		}
		return fibonacci;
	}
	
	@Override
	public void done()
	{		
		 if (isCancelled())
			 resultJTextField.setText("Cancelado");
		 else
		 {
			 try {
				resultJTextField.setText( get().toString() );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			 barraProgreso.setIndeterminate(false);
		 }
	}
	
	@Override 
    protected void process(List<Long> data) 
	{
	    for (Long actual: data)
	    {
            resultJTextField.setText(Long.toString(actual));
	    }
    }
}
