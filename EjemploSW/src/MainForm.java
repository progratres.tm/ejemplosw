import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import java.awt.Font;

public class MainForm 
{
	private JFrame frame;
	private JTextField textFieldFibo;
	private JProgressBar progressBar;
	private Simulacion simulacion=null;
	private Simulacion2 simulacion2=null;
	private int numero = 0;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 594, 375);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblResultado.setBounds(35, 90, 114, 14);
		frame.getContentPane().add(lblResultado);
		
		JTextField textFieldResultado = new JTextField();
		textFieldResultado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textFieldResultado.setEditable(false);
		textFieldResultado.setBounds(159, 89, 132, 20);
		frame.getContentPane().add(textFieldResultado);
		textFieldResultado.setColumns(10);
		
		JLabel lblFibonacciDe = new JLabel("Fibonacci de...");
		lblFibonacciDe.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFibonacciDe.setBounds(35, 31, 150, 27);
		frame.getContentPane().add(lblFibonacciDe);
		
		textFieldFibo = new JTextField();
		textFieldFibo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textFieldFibo.setEditable(false);
		textFieldFibo.setBounds(161, 34, 86, 20);
		frame.getContentPane().add(textFieldFibo);
		textFieldFibo.setColumns(10);
		
		JButton btnNumero = new JButton("Numero de Fibonacci");
		btnNumero.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNumero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				textFieldResultado.setText(" ");
				textFieldFibo.setText(" ");
				numero = Integer.parseInt(JOptionPane.showInputDialog("Que numero de Fibo queres conocer?"));
				textFieldFibo.setText(Integer.toString(numero));
			}
		});
		btnNumero.setBounds(385, 26, 161, 41);
		frame.getContentPane().add(btnNumero);
		
		JButton btnGo = new JButton("Recursivo SwingWorker");
		btnGo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				textFieldResultado.setText(" ");
				simulacion = new Simulacion(textFieldResultado, progressBar, numero);
				simulacion.execute();
			}
		});
		btnGo.setBounds(35, 139, 161, 41);
		frame.getContentPane().add(btnGo);
		
		JButton btnGo_1 = new JButton("Recursivo");
		btnGo_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnGo_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				textFieldResultado.setText(" ");
				long resultado = Fibonacci.fibonacci(numero);
				textFieldResultado.setText(Long.toString(resultado));
			}
		});
		btnGo_1.setBounds(206, 139, 169, 41);
		frame.getContentPane().add(btnGo_1);
		
		JButton btnMuestra = new JButton("Iterativo");
		btnMuestra.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnMuestra.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				textFieldResultado.setText(" ");				
				simulacion2 = new Simulacion2(textFieldResultado, progressBar, numero);
				simulacion2.execute();
			}
		});
		btnMuestra.setBounds(385, 139, 161, 41);
		frame.getContentPane().add(btnMuestra);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if (simulacion!=null && simulacion.isDone()==false)
					simulacion.cancel(true);
					textFieldResultado.setText("Interrumpido");
					progressBar.setIndeterminate(false);
				if (simulacion2!=null && simulacion2.isDone()==false)
					simulacion2.cancel(true);
					textFieldResultado.setText("Interrumpido");
					progressBar.setIndeterminate(false);					
			}
		});
		btnCancelar.setBounds(206, 263, 169, 41);
		frame.getContentPane().add(btnCancelar);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(35, 206, 511, 27);
		frame.getContentPane().add(progressBar);
	}
}