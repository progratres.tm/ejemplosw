package wbEjemplo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class VentanaPrincipal {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal window = new VentanaPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(135, 86, 187, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("New button");
	
		btnNewButton.setBounds(198, 204, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(10, 143, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(119, 143, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(236, 143, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(236, 174, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double tp1 = Double.parseDouble(textField.getText());
				double tp2 = Double.parseDouble(textField_1.getText());
				double tp3 = Double.parseDouble(textField_2.getText());
				double tp4 = Double.parseDouble(textField_3.getText());
				
				lblNewLabel.setText(" " + aprobacion(tp1, tp2, tp3, tp4));
			}
		});
		
	}
	
	public enum Estado { Recursa, Regulariza, Promociona };
	static Estado aprobacion(double tp1, double tp2, double tp3, double parcial)
	{
	if( tp1 < 4 || tp2 < 4 || tp3 < 4 || parcial < 4 ) 
			 return Estado.Recursa;
	double promedioTPs = (tp1 + tp2 + tp3) / 3; 
	double promedio = (promedioTPs + parcial) / 2; 
	return promedio >= 7 ? Estado.Promociona : Estado.Regulariza; 
	}
}
