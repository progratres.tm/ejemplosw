package wbEjemplo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.JLabel;

public class Ventana {

	private JFrame frame;
	private JTextField textField_tp1;
	private JTextField textField_1_tp2;
	private JTextField textField_2_tp3;
	private JTextField textField_parcial;
	private JButton btn_calcular;
	
	JLabel lblNewLabel;
	private JTextField textField;
	private JLabel label_5;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		btn_calcular = new JButton("Calcular");
		btn_calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
				
				double tp1 = Double.parseDouble(textField_tp1.getText());
				double tp2 = Double.parseDouble(textField_2_tp3.getText());
				double tp3 = Double.parseDouble(textField_1_tp2.getText());
				double tp4 = Double.parseDouble(textField_parcial.getText());
				
				lblNewLabel.setText(" " + aprobacion(tp1, tp2, tp3, tp4));
				
						
			
				
			}
		});
		btn_calcular.setBounds(141, 217, 91, 23);
		frame.getContentPane().add(btn_calcular);
		
		textField_tp1 = new JTextField();
		textField_tp1.setBounds(56, 141, 31, 39);
		frame.getContentPane().add(textField_tp1);
		textField_tp1.setColumns(10);
		
		textField_1_tp2 = new JTextField();
		textField_1_tp2.setColumns(10);
		textField_1_tp2.setBounds(141, 141, 31, 39);
		frame.getContentPane().add(textField_1_tp2);
		
		textField_2_tp3 = new JTextField();
		textField_2_tp3.setColumns(10);
		textField_2_tp3.setBounds(216, 141, 31, 39);
		frame.getContentPane().add(textField_2_tp3);
		
		 lblNewLabel = new JLabel("Resultado");
		lblNewLabel.setBounds(170, 70, 62, 23);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel label = new JLabel("TP1");
		label.setBounds(56, 121, 46, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("TP2");
		label_1.setBounds(141, 121, 46, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("TP3");
		label_2.setBounds(206, 121, 46, 14);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Ingrese sus notas si se atreve...");
		label_3.setBounds(89, 23, 256, 23);
		frame.getContentPane().add(label_3);
		
		textField_parcial = new JTextField();
		textField_parcial.setColumns(10);
		textField_parcial.setBounds(292, 141, 31, 39);
		frame.getContentPane().add(textField_parcial);
		
		JLabel label_4 = new JLabel("Parcial");
		label_4.setBounds(292, 121, 46, 14);
		frame.getContentPane().add(label_4);
		
		textField = new JTextField();
		textField.setBounds(333, 150, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		label_5 = new JLabel("Parcial 2");
		label_5.setBounds(350, 121, 69, 14);
		frame.getContentPane().add(label_5);
		
		
		
		
		
		
		
		
		
	}
	
	
	public enum Estado { Recursa, Regulariza, Promociona };
	static Estado aprobacion(double tp1, double tp2, double tp3, double parcial)
	{
	if( tp1 < 4 || tp2 < 4 || tp3 < 4 || parcial < 4 ) 
			 return Estado.Recursa;
	double promedioTPs = (tp1 + tp2 + tp3) / 3; 
	double promedio = (promedioTPs + parcial) / 2; 
	return promedio >= 7 ? Estado.Promociona : Estado.Regulariza; 
	
	
	
	}
	
	
}
