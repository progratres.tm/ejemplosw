
public class Fibonacci 
{
	public static long fibonacci(long number)
	{
	   if ( number == 0 || number == 1 )
	      return number;
	      
	   return fibonacci( number - 1 ) + fibonacci( number - 2 );
	}
}
