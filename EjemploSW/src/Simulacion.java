import java.util.concurrent.ExecutionException;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class Simulacion extends SwingWorker<Long, Long>
{
	private int _n; 
	private JTextField resultJTextField; 
	private JProgressBar barraProgreso;
	
	public Simulacion(JTextField result, JProgressBar barra, int numero)
	{
		resultJTextField = result;
		barraProgreso = barra;
		_n = numero;
	}
	
	@Override
	protected Long doInBackground() throws Exception 
	{
		barraProgreso.setIndeterminate(true);
		long nthFib = Fibonacci.fibonacci(_n);
		return nthFib;
	}
	
	@Override
	public void done()
	{
	   try
	   {
	      // get el resultado de doInBackground y lo mostramos, si es que no nos cancelaron
		   if (this.isCancelled()==false)
		   {
			   resultJTextField.setText( get().toString() );
			   barraProgreso.setIndeterminate(false);
		   }
	   } 
	   catch ( InterruptedException ex ) 
	   {
		   resultJTextField.setText( "Interrumpido mientras esperaba resultados." );
	   } 
	   catch ( ExecutionException ex ) 
	   {
		   resultJTextField.setText( "Error mientras se realizaba el calculo" );
	   } 
	} 
}